import static org.junit.Assert.*;

import org.junit.Test;

public class CalculadoraTest {

	@Test
	public void testSuma() {
		int resultado=Calculadora.suma(3, 2);
		int resEsperado=5;
		assertEquals(resEsperado,resultado);
	}
	@Test
	public void testSumaNegativos() {
		int resultado=Calculadora.suma(-4, -6);
		int resEsperado=-10;
		assertEquals(resEsperado,resultado);
	}
	@Test
	public void testSumaPosNeg() {
		int resultado=Calculadora.suma(3, -2);
		int resEsperado=1;
		assertEquals(resEsperado,resultado);
	}
	@Test
	public void testResta() {
		int resultado=Calculadora.resta(3,2);
		int resEsperado=1;
		assertEquals(resEsperado,resultado);
	}
	@Test
	public void testRestaNegativos() {
		int resultado=Calculadora.resta(-5,-5);
		int resEsperado=0;
		assertEquals(resEsperado,resultado);
	}
	@Test
	public void testRestaNegPos() {
		int resultado=Calculadora.resta(6,-5);
		int resEsperado=11;
		assertEquals(resEsperado,resultado);
	}
	@Test
	public void  testMultiplicar() {
		int resultado=Calculadora.multiplicar(2,2);
		int resEsperado=4;
		assertEquals(resEsperado,resultado);
	}
	@Test
	public void  testMultiplicarNeg() {
		int resultado=Calculadora.multiplicar(-2,-2);
		int resEsperado=4;
		assertEquals(resEsperado,resultado);
	}
	@Test
	public void  testMultiplicarPosNeg() {
		int resultado=Calculadora.multiplicar(-3,2);
		int resEsperado=-6;
		assertEquals(resEsperado,resultado);
	}
	@Test
	public void  testDividir() {
		int resultado=Calculadora.dividir(6,2);
		int resEsperado=3;
		assertEquals(resEsperado,resultado);
	}
	@Test
	public void  testDividirPosNeg() {
		int resultado=Calculadora.dividir(-6,2);
		int resEsperado=-3;
		assertEquals(resEsperado,resultado);
	}
	@Test
	public void  testDividirNeg() {
		int resultado=Calculadora.dividir(-3,-3);
		int resEsperado=1;
		assertEquals(resEsperado,resultado);
	}
}
